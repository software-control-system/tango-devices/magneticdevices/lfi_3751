//=============================================================================
//
// file :         LFI_3751Class.h
//
// description :  Include for the LFI_3751Class root class.
//                This class is represents the singleton class for
//                the LFI_3751 device class.
//                It contains all properties and methods which the 
//                LFI_3751 requires only once e.g. the commands.
//			
// project :      TANGO Device Server
//
// $Author:  $
//
// $Revision:  $
//
// $Log:  $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#ifndef _LFI_3751CLASS_H
#define _LFI_3751CLASS_H

#include <tango.h>
#include <LFI_3751.h>


namespace LFI_3751_ns
{
//=====================================
//	Define classes for attributes
//=====================================
class outputCurrentAttrib: public Tango::Attr
{
public:
	outputCurrentAttrib():Attr("outputCurrent", Tango::DEV_BOOLEAN, Tango::READ_WRITE) {};
	~outputCurrentAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<LFI_3751 *>(dev))->read_outputCurrent(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<LFI_3751 *>(dev))->write_outputCurrent(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<LFI_3751 *>(dev))->is_outputCurrent_allowed(ty);}
};

class coeffDAttrib: public Tango::Attr
{
public:
	coeffDAttrib():Attr("coeffD", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~coeffDAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<LFI_3751 *>(dev))->read_coeffD(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<LFI_3751 *>(dev))->write_coeffD(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<LFI_3751 *>(dev))->is_coeffD_allowed(ty);}
};

class coeffIAttrib: public Tango::Attr
{
public:
	coeffIAttrib():Attr("coeffI", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~coeffIAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<LFI_3751 *>(dev))->read_coeffI(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<LFI_3751 *>(dev))->write_coeffI(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<LFI_3751 *>(dev))->is_coeffI_allowed(ty);}
};

class coeffPAttrib: public Tango::Attr
{
public:
	coeffPAttrib():Attr("coeffP", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~coeffPAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<LFI_3751 *>(dev))->read_coeffP(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<LFI_3751 *>(dev))->write_coeffP(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<LFI_3751 *>(dev))->is_coeffP_allowed(ty);}
};

class temperatureErrorAttrib: public Tango::Attr
{
public:
	temperatureErrorAttrib():Attr("temperatureError", Tango::DEV_DOUBLE, Tango::READ) {};
	~temperatureErrorAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<LFI_3751 *>(dev))->read_temperatureError(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<LFI_3751 *>(dev))->is_temperatureError_allowed(ty);}
};

class heaterPercentAttrib: public Tango::Attr
{
public:
	heaterPercentAttrib():Attr("heaterPercent", Tango::DEV_DOUBLE, Tango::READ) {};
	~heaterPercentAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<LFI_3751 *>(dev))->read_heaterPercent(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<LFI_3751 *>(dev))->is_heaterPercent_allowed(ty);}
};

class resistanceMeasuredAttrib: public Tango::Attr
{
public:
	resistanceMeasuredAttrib():Attr("resistanceMeasured", Tango::DEV_DOUBLE, Tango::READ) {};
	~resistanceMeasuredAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<LFI_3751 *>(dev))->read_resistanceMeasured(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<LFI_3751 *>(dev))->is_resistanceMeasured_allowed(ty);}
};

class temperatureMeasuredAttrib: public Tango::Attr
{
public:
	temperatureMeasuredAttrib():Attr("temperatureMeasured", Tango::DEV_DOUBLE, Tango::READ) {};
	~temperatureMeasuredAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<LFI_3751 *>(dev))->read_temperatureMeasured(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<LFI_3751 *>(dev))->is_temperatureMeasured_allowed(ty);}
};

class temperaturePresetAttrib: public Tango::Attr
{
public:
	temperaturePresetAttrib():Attr("temperaturePreset", Tango::DEV_DOUBLE, Tango::READ_WRITE) {};
	~temperaturePresetAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<LFI_3751 *>(dev))->read_temperaturePreset(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<LFI_3751 *>(dev))->write_temperaturePreset(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<LFI_3751 *>(dev))->is_temperaturePreset_allowed(ty);}
};

//=========================================
//	Define classes for commands
//=========================================
class PidAutotuneDCmd : public Tango::Command
{
public:
	PidAutotuneDCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	PidAutotuneDCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~PidAutotuneDCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<LFI_3751 *>(dev))->is_PidAutotuneD_allowed(any);}
};



class PidAutotuneSCmd : public Tango::Command
{
public:
	PidAutotuneSCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	PidAutotuneSCmd(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~PidAutotuneSCmd() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<LFI_3751 *>(dev))->is_PidAutotuneS_allowed(any);}
};



//
// The LFI_3751Class singleton definition
//

class LFI_3751Class : public Tango::DeviceClass
{
public:

//	add your own data members here
//------------------------------------

public:
	Tango::DbData	cl_prop;
	Tango::DbData	cl_def_prop;
	Tango::DbData	dev_def_prop;

//	Method prototypes
	static LFI_3751Class *init(const char *);
	static LFI_3751Class *instance();
	~LFI_3751Class();
	Tango::DbDatum	get_class_property(string &);
	Tango::DbDatum	get_default_device_property(string &);
	Tango::DbDatum	get_default_class_property(string &);
	
protected:
	LFI_3751Class(string &);
	static LFI_3751Class *_instance;
	void command_factory();
	void attribute_factory(vector<Tango::Attr *> &);
	void write_class_property();
	void set_default_property();

private:
	void device_factory(const Tango::DevVarStringArray *);
};


}	//	namespace LFI_3751_ns

#endif // _LFI_3751CLASS_H
